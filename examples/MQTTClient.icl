module MQTTClient

import iTasks.Extensions.MQTT
import iTasks.Extensions.MQTT.Client
import iTasks.Extensions.MQTT.Util
import iTasks
import TCPDef
import StdDebug

defaultSettings = 
  { MQTTConnectionSettings
  | host         = "localhost"
  , port         = 1883
  , clientId     = "Mqtt client"
  , keepAlive    = 60
  , cleanSession = True
  , auth         = NoAuth
  , lwt          = ?Just (MQTTMsg "last-will-topic" "error" { qos=1, retain=False })
  }

Start world = doTasks demo world

demo :: Task ()
demo = getSettings >>- \conSettings. mqttConnect conSettings controls

getSettings :: Task MQTTConnectionSettings
getSettings = updateInformation [] defaultSettings >>*
  [OnAction ActionOk  (hasValue return)]

controls :: (SimpleSDSLens MQTTClient) -> Task ()
controls sds = ((send sds <<@ Title "Send") -&&- (subscribe sds <<@ Title "Subscribe") -&&- 
  (unsubscribe sds <<@ Title "Unsubscribe") -&&- (disconnect sds <<@ Title "Disconnect") -&&-
  (received sds <<@ Title "Received")) @! ()

send :: (SimpleSDSLens MQTTClient) -> Task MQTTMsg
send sds = enterInformation [] >>*
  [OnAction ActionOk  (hasValue (\msg. (mqttSend msg sds) >-| send sds))]

subscribe :: (SimpleSDSLens MQTTClient) -> Task (MQTTTopicFilter, QoS)
subscribe sds = enterInformation [] >>*
  [OnAction ActionOk  (hasValue (\sub. (mqttSubscribe sub sds) >-| subscribe sds))]
  
unsubscribe :: (SimpleSDSLens MQTTClient) -> Task MQTTTopicFilter
unsubscribe sds = enterInformation [] >>*
  [OnAction ActionOk  (hasValue (\sub. (mqttUnsubscribe sub sds) >-| unsubscribe sds))]
  
disconnect :: (SimpleSDSLens MQTTClient) -> Task ()
disconnect sds = viewInformation [] () >>*
  [OnAction (Action "Disconnect") (always (mqttDisconnect sds >-| disconnect sds))]
  
received :: (SimpleSDSLens MQTTClient) -> Task [MQTTMsg]
received sds
  # lens = createReceiveLens sds
  = viewSharedInformation [ViewAs \res.map format res] lens
  where format (MQTTMsg t p opts) = "Topic: " +++ t +++ " Payload: " +++ p +++ " Qos: " +++ (toString opts.qos) +++ " Retained: " +++ (toString opts.retain)
