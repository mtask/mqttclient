definition module iTasks.Extensions.MQTT

import iTasks
import iTasks.WF.Definition
import TCPDef

:: MQTTConnectionSettings = 
  { host         :: String
  , port         :: Port
  , clientId     :: ClientId
  , keepAlive    :: Int
  , cleanSession :: Bool
  , auth         :: MQTTAuth
  , lwt          :: ?MQTTMsg
  }
  
:: MQTTAuth 
   = NoAuth
   | UsernamePassword String String

:: ClientId :== String

:: MQTTMsg = MQTTMsg MQTTTopic MQTTPayload MQTTMsgOpts
:: MQTTTopic :== String
:: MQTTPayload :== String

:: QoS :== Int
:: MQTTMsgOpts =
  { qos :: QoS
  , retain :: Bool
  }

:: MQTTTopicFilter :== String
:: MQTTSubscribe :== (MQTTTopicFilter, QoS)

:: MQTTClient =
  { received    :: [MQTTMsg]
  , send        :: [MQTTMsg]
  , subscribe   :: [MQTTSubscribe]
  , unsubscribe :: [MQTTTopicFilter]
  , lastMessage :: Timestamp
  , disconnect  :: Bool
  , context     :: Int // Pointer to the WolfMQTT MqttClient
  }

derive class iTask MQTTMsg, MQTTMsgOpts, MQTTConnectionSettings, MQTTAuth, MQTTClient
