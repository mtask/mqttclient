implementation module iTasks.Extensions.MQTT.Util

import iTasks
import iTasks.Extensions.MQTT

createReceiveLens :: !(SimpleSDSLens MQTTClient) -> (SimpleSDSLens [MQTTMsg])
createReceiveLens sds = mapReadWrite (read, write) ?None sds
  where read r = r.received
        write received c = (?Just { c & received = received })

mqttSend :: !MQTTMsg !(SimpleSDSLens MQTTClient) -> Task ()
mqttSend msg sds = upd (\c. { c & send = c.send ++ [msg] }) sds @! ()

mqttSubscribe :: !MQTTSubscribe !(SimpleSDSLens MQTTClient) -> Task ()
mqttSubscribe  sub sds = upd (\c. { c & subscribe = c.subscribe ++ [sub] }) sds @! ()

mqttUnsubscribe :: !MQTTTopicFilter !(SimpleSDSLens MQTTClient) -> Task ()
mqttUnsubscribe sub sds = upd (\c. { c & unsubscribe = c.unsubscribe ++ [sub] }) sds @! ()

mqttDisconnect :: !(SimpleSDSLens MQTTClient) -> Task ()
mqttDisconnect sds = upd (\c. { c & disconnect = True }) sds @! ()
