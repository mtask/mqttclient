implementation module iTasks.Extensions.MQTT.Client

import code from "libWrapper."

import code from "libwolfmqtt_la-mqtt_client."
import code from "libwolfmqtt_la-mqtt_packet."
import code from "libwolfmqtt_la-mqtt_socket."

import iTasks.Extensions._MQTT

import qualified Data.Map as DM
import Data.Functor
import Data.Func

import iTasks
import iTasks.WF.Definition
import iTasks.Internal.IWorld
import iTasks.Internal.DynamicUtil
import iTasks.Internal.Task
import iTasks.Internal.TaskEval
import iTasks.Internal.Util
import iTasks.Internal.TaskServer
import iTasks.Internal.SDS
import iTasks.SDS.Definition

import iTasks.Extensions.DateTime

import TCPChannelClass
import tcp
import TCPIP
import TCPDef

import System._Pointer
import _SystemArray
import StdMisc

import iTasks.Extensions.MQTT.Errors
import iTasks.Extensions.MQTT

import StdDebug

mqttConnect :: MQTTConnectionSettings ((SimpleSDSLens MQTTClient) -> Task a) -> Task a | iTask a
mqttConnect conSettings ftask =
  defaultClient >>- \client.
  withShared client \sds.
    (parallel
      [ (Embedded, \_.    (ftask sds) @ ?Just)
      , (Embedded, (\stl. (appendTask Embedded (\_. connectionTask conSettings sds  @! ?None) stl) @! ?None))
      , (Embedded, \_.    (pingBroker sds conSettings.keepAlive) @! ?None)
      , (Embedded, \stl.  (onTaskCompleteHandler sds stl) @! ?None)
      ] [] @? \tv. case tv of
        NoValue = NoValue
        Value pv _ = case findTaskVal pv of
          ?Just (v, s) = Value v s
          _            = NoValue)

  where findTaskVal [(_, Value (?Just val) s):_] = ?Just (val, s)
        findTaskVal [_:tv]                       = findTaskVal tv
        findTaskVal []                           = ?None

onTaskCompleteHandler :: (SimpleSDSLens MQTTClient) (SharedTaskList a) -> Task MQTTClient | iTask a
onTaskCompleteHandler sds stl
  = watch (sdsFocus (Left 0) (taskListItemValue stl)) >>*
  [OnValue $ ifValue isStable (\_. upd (\c. { c & disconnect=True }) sds) ]
  where
    isStable (Value _ s) = s
    isStable NoValue     = False

defaultClient :: Task MQTTClient
defaultClient = get currentTimestamp >>- \ts. return
  { MQTTClient
  | send = []
  , received = []
  , subscribe = []
  , unsubscribe = []
  , lastMessage = ts
  , disconnect = False
  , context = 0
  }

connectionTask :: MQTTConnectionSettings (SimpleSDSLens MQTTClient) -> Task ()
connectionTask conSettings sds = Task evalinit
  where evalinit event _ iworld
		    | isDestroyOrInterrupt event
			  = (DestroyedResult, iworld)
      	evalinit event eo=:{TaskEvalOpts|taskId} iworld
      		= case addIOTask taskId (toDynamic sds) initConnection (ioOps 0) onInitHandler (mkIOTaskInstance taskId) iworld of
            (Error e,iworld) = (ExceptionResult e, iworld)
            (Ok _,iworld) = eval event eo iworld
        eval event {TaskEvalOpts|taskId} iworld=:{ioStates}
    		  | isDestroyOrInterrupt event
    			# ioStates = case 'DM'.get taskId ioStates of
    				?Just (IOActive values) = 'DM'.put taskId (IODestroyed values) ioStates
    				_                       = ioStates
    			= (DestroyedResult,{iworld & ioStates = ioStates})
        eval event evalOpts=:{TaskEvalOpts|taskId,lastEval} iworld=:{ioStates}
      		= case 'DM'.get taskId ioStates of
      			?None = (ValueResult NoValue (mkTaskEvalInfo lastEval) NoChange (Task eval), iworld)
      			?Just (IOActive values)
      				= case 'DM'.get 0 values of
      					?Just (l :: l^, s)
      						= (ValueResult (Value l s) (mkTaskEvalInfo lastEval) NoChange (Task eval), iworld)
      					?None = (DestroyedResult ,iworld)
      			?Just (IOException e)
      				= (ExceptionResult (exception e),iworld)
        initConnection :: !*IWorld -> (!MaybeErrorString ((!Int, !IPAddress), !*TCP_DuplexChannel), !*IWorld)
        initConnection w =
          case resolveIP conSettings w of
            (Ok ip, w) = case createTCPConnection conSettings ip w of
                (Ok (ref, chl), w) = case createMQTTConnection conSettings ref w of
                    (Ok ctx, w) = case modify (\c.{ c & context = ctx }) sds EmptyContext w of
                        (Ok _, w) = (Ok ((ctx, ip), chl), w)
                        (Error (_, e), w) = (Error e, w)
                    (Error e, w) = (Error e, w)
                (Error e, w) = (Error e, w)
            (Error e, w) = (Error e, w)
        onInitHandler connId _ _ w = (toDyn <$> Ok (), ?None , [], False, w)
        mkIOTaskInstance taskId connectionId (ctx, ip) channel
      		# opts = {ConnectionInstanceOpts|taskId = taskId, connectionId = connectionId, remoteHost = ip, connectionTask = (wrapIWorldConnectionTask connectionsHandlers sds), ioOps = (ioOps ctx), removeOnClose = True}
      		= ConnectionInstance opts channel

// Connection handlers
connectionsHandlers :: ConnectionHandlersIWorld () MQTTClient MQTTClient
connectionsHandlers =
  { ConnectionHandlersIWorld
  | onConnect = \_ _ _ w. (Ok (), ?None, [], False, w)
  , onData = onData
  , onShareChange = onShareChange
  , onTick = \_ _ w. (Ok (), ?None, [], False, w)
  , onDisconnect = \_ _ _ w. (Ok (), ?None, w)
  , onDestroy = \_ w. (Ok (), [], w)
  }

onData :: !ConnectionId !String () MQTTClient *IWorld -> *(MaybeErrorString (), ?MQTTClient, [String], Bool, *IWorld)
onData _ _ _ client iworld
	# (ret, iworld) = processCall client.context iworld
	| ret == 0 = let (c,w) = receive client iworld in (Ok (), ?Just c, [], False, w)
	| otherwise = (Error $ "Mqtt process failed: " +++ errorCodeToString ret, ?None, [], False, iworld)

onShareChange :: !() MQTTClient *IWorld -> *(MaybeErrorString (), ?MQTTClient, [String], Bool, *IWorld)
onShareChange _ c=:{subscribe=[(topic, qos):tt]} w=:{IWorld|clock}
  # (ret, w) = (subscribeCall c.context (packString topic) qos w)
  # c = { c & subscribe = tt, lastMessage = timespecToStamp clock}
  | ret == 0 = let (cl,iw) = receive c w in (Ok (), ?Just cl, [], False, iw)
  | otherwise = (Error $ "Subscription failed: " +++ errorCodeToString ret, ?None, [], False, w)
onShareChange _ c=:{unsubscribe=[topic:tt]} w=:{IWorld|clock}
  # (ret, w) = (unsubscribeCall c.context (packString topic) w)
  # c = { c & unsubscribe = tt, lastMessage = timespecToStamp clock}
  | ret == 0 = let (cl,iw) = receive c w in (Ok (), ?Just cl, [], False, iw)
  | otherwise =  (Error $ "Unsubscription failed: " +++ errorCodeToString ret, ?None, [], False, w)
onShareChange _ c=:{send=[(MQTTMsg t p opts):ss]} w=:{IWorld|clock}
  # (ret, w) = (publishCall c.context (packString t) p (size p) opts.qos opts.retain w)
  # c = { c & send = ss, lastMessage = timespecToStamp clock }
  | ret == 0 = let (cl,iw) = receive c w in (Ok (), ?Just cl, [], False, iw)
  | otherwise = (Error $ "Send failed: " +++ errorCodeToString ret, ?None, [], False, w)
onShareChange _ c=:{disconnect = True} w
  = (Ok (), ?None, [], True, w)
onShareChange _ _ w = (Ok (), ?None, [], False, w) // Nothing to do

// Ping task
pingBroker :: (SimpleSDSLens MQTTClient) Int -> Task (?Int)
pingBroker sds keepalive | keepalive <= 2 = throw "Keep alive time must be greater than 2 seconds."
pingBroker sds keepalive = foreverIf ((==) (?Just 0)) $
  get sds >>-
  \client. timestampToLocalDateTime (nextFire client.lastMessage) >>-
  \nf. waitForDateTime False nf >>-
  \dt. localDateTimeToTimestamp dt >>-
  \now. get sds >>-
  \client. handlePing client now
  where nextFire (Timestamp lm) = Timestamp (lm + (keepalive - 2))
        threshold (Timestamp now) (Timestamp lm) = (lm + (keepalive - 2)) - now <= 10
        handlePing client now | client.disconnect = return ?None
                              | (threshold now client.lastMessage) = sendPing client now
                              | otherwise = return (?Just 0)
        sendPing client now = upd (\c. { c & lastMessage = now}) sds >-| accWorld (pingCall client.context) >>- \res.
                              case res of
                                0   = return (?Just 0)
                                ret = throw $ "Failed to send ping: " +++ errorCodeToString ret


// IO Task Operations that handle the low-level read, write and close operations.
ioOps :: Int -> IOTaskOperations *TCP_DuplexChannel String ()
ioOps ctx = {readData = readData, writeData = writeData, closeIO = closeIO}
  where
  	readData i selects (chl, w)
      = case getSelect i selects of
          ?Just SR_Available = (IODData "", chl, w) // The actual read is in the onData handler
          ?None              = (IODNoData, chl, w)
          _                  = (IODClosed (), chl, w)
    writeData _ (chl, w) = (chl, w)
  	closeIO ({rChannel, sChannel}, iworld=:{world})
      # (ret, iworld=:{world}) = disconnectCall ctx iworld
      # world = closeRChannel rChannel world
      # world = closeChannel sChannel world
      = {iworld & world = world}

getSelect :: Int ![(Int,SelectResult)] -> ? SelectResult
getSelect i [(iden,sr)] | (i == iden) = ?Just sr
                        | otherwise   = ?None
getSelect i _           = ?None

// Receive messages if they are available
receive :: MQTTClient !*IWorld -> (MQTTClient, !*IWorld)
receive client iworld
    # (ret, topic, payload, qos, retain, iworld) = receiveCall client.context iworld
    # msg = MQTTMsg topic payload { qos=qos, retain=retain }
    | ret == 0 = receive { client & received = [msg:client.received] } iworld
    | ret == -800 = (client, iworld) // No messages in queue


// Resolve the ip address
resolveIP :: MQTTConnectionSettings !*IWorld -> (!MaybeErrorString IPAddress, !*IWorld)
resolveIP conSettings iworld
    # host = conSettings.MQTTConnectionSettings.host
    # (ip, w) = lookupIPAddress host iworld.world
    = case ip of
        ?Just ip = (Ok ip, {iworld & world = w})
        ?None    = (Error $ host +++ " could not be resolved.", {iworld & world = w})

// Create the TCP connection
createTCPConnection :: MQTTConnectionSettings IPAddress !*IWorld -> (!MaybeErrorString (!EndpointRef, !*TCP_DuplexChannel), !*IWorld)
createTCPConnection conSettings ip iworld
        = case connectTCP_MT (?Just conSettings.MQTTConnectionSettings.keepAlive) (ip, conSettings.MQTTConnectionSettings.port) iworld.world of
            (_, (?Just con=:{rChannel}), w)
                      # (ref, i) = unpack_tcprchan rChannel
                      = (Ok (ref, {con & rChannel=pack_tcprchan (ref, i)}), {iworld & world = w})
            (_, _, w) = (Error "The TCP connection could not be created.", {iworld & world = w} )


// Create the MQTT connection
createMQTTConnection :: MQTTConnectionSettings !EndpointRef !*IWorld -> (!MaybeErrorString Int, !*IWorld)
createMQTTConnection conSettings ref w
        # auth         = conSettings.MQTTConnectionSettings.auth
        # (context, ret, w)     = call auth
        = case ret of
            0   = (Ok context, w)
            ret = (Error $ "Could not connect to the MQTT broker: " +++ errorCodeToString ret , w)
        where
          clientId            = conSettings.MQTTConnectionSettings.clientId
          keepAlive           = conSettings.MQTTConnectionSettings.keepAlive
          cleanSession        = conSettings.MQTTConnectionSettings.cleanSession
          lw                  = conSettings.MQTTConnectionSettings.lwt
          (MQTTMsg lwt lwm opts)   = if (isJust lw) (fromJust lw) (MQTTMsg "" "" { qos=0, retain=False })
          call NoAuth = connectCall ref (packString clientId) keepAlive cleanSession (packString lwt) (packString lwm) opts.qos opts.retain w
          call (UsernamePassword u p) = connectAuthCall ref (packString clientId) keepAlive cleanSession (packString u) (packString p) (packString lwt) (packString lwm) opts.qos opts.retain w

// Interface to WolfMQTT
connectCall :: !Int !{#Char} !Int !Bool !{#Char} !{#Char} !Int !Bool !*IWorld -> *(!Int, !Int, !*IWorld)
connectCall fd ci ka cs lwt lwm qos ret w = code {
    ccall mqtt_connect_without_auth "IsIIssII:pI:A"
}

connectAuthCall :: !Int !{#Char} !Int !Bool !{#Char} !{#Char} !{#Char} !{#Char} !Int !Bool !*IWorld -> *(!Int, !Int, !*IWorld)
connectAuthCall fd ci ka cs u p lwt lwm qos ret w = code {
    ccall mqtt_connect "IsIIssssII:pI:A"
}

disconnectCall :: !Int !*IWorld -> *(!Int, !*IWorld)
disconnectCall c w = code {
    ccall mqtt_disconnect "p:I:A"
}

pingCall :: !Int !*World -> *(!Int, !*World)
pingCall c w = code {
      ccall mqtt_ping "p:I:A"
  }

publishCall :: !Int !{#Char} !{#Char} !Int !Int !Bool !*IWorld -> *(!Int, !*IWorld)
publishCall c topic msg len qos retain w = code {
      ccall mqtt_publish "pssIII:I:A"
  }

subscribeCall :: !Int !{#Char} !Int !*IWorld -> *(!Int, !*IWorld)
subscribeCall c topic qos w = code {
    ccall mqtt_subscribe "psI:I:A"
}

unsubscribeCall :: !Int !{#Char} !*IWorld -> *(!Int, !*IWorld)
unsubscribeCall c topic w = code {
    ccall mqtt_unsubscribe "ps:I:A"
}

processCall :: !Int !*IWorld -> *(!Int, !*IWorld)
processCall c w = code {
    ccall mqtt_process "p:I:A"
}

receiveCall :: !Int !*IWorld -> *(!Int, !String, !String, !Int, !Bool, !*IWorld)
receiveCall c w = code {
    ccall mqtt_receive "I:VISSII:A"
}
